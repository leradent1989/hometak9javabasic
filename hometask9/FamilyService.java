package hometask9;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.chrono.ChronoLocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class FamilyService {
    private CollectionFamilyDao familyDao;

    public FamilyService(CollectionFamilyDao familyDao) {
        this.familyDao = familyDao;
    }
//1
public List <Family> getAllFamilies() {

    List<Family> families1 = familyDao.findAll();
   return families1;
}
    //2
    public void displayAllFamilies() {

        List<Family> families = familyDao.findAll();
        families.forEach(family -> family.toString());
    }

    //3
    public Family getFamilyById(int index) {

        Family family = familyDao.getFamilyByIndex(index);

        return family;
    }

    //4
    public int count() {
        List<Family> families = familyDao.findAll();
        int listLength = families.size();
        return listLength;
    }

    //5
    public void createNewFamily(Human mother, Human father) {
        Family newFamily = new Family(mother, father);
        familyDao.save(newFamily);
    }

    //6
    public boolean deleteFamilyByIndex(int index) {
        boolean result = familyDao.deleteFamily(index);
        return result;

    }

    //7
    public Set<Pet> getPets(int index) {
        Family family = familyDao.getFamilyByIndex(index);
        Set<Pet> pets = family.getPet();
        return pets;
    }

    //8
    public void addPet(int index, Pet pet) {
        Family family = familyDao.getFamilyByIndex(index);
        Set<Pet> pets = family.getPet();
        pets.add(pet);
    }

    //9
    public List<Family> getFamiliesBiggerThen(int size) {
        List<Family> families = familyDao.findAll();
        List<Family> newFamilies = new ArrayList<>();
        families.forEach(family -> {
            if (family.countFamily(family.getChildren()) > size) {
                newFamilies.add(family);
            }

        });
        return newFamilies;
    }
    //10
    public int   countFamiliesWithMemberNumber (int size) {
        List<Family> families = familyDao.findAll();
        List<Family> newFamilies = new ArrayList<>();
        families.forEach(family -> {
            if (family.countFamily(family.getChildren()) == size) {
                newFamilies.add(family);
            }

        });
        return newFamilies.size();


    }
    //11
    public List<Family> getFamiliesLessThen(int size) {
        List<Family> families = familyDao.findAll();
        List<Family> newFamilies = new ArrayList<>();
        families.forEach(family -> {
            if (family.countFamily(family.getChildren()) < size) {
                newFamilies.add(family);
            }

        });
        return newFamilies;    }
    //12
    public Family  adoptChild(Family family,Human child){
        List<Family> families = familyDao.findAll();
       int index = families.indexOf(family);
       Family currentFamily = families.get(index);
       currentFamily.addChild(child);
       return currentFamily;
    }
//13
    public Family bornChild(Family family,String girlName,String boyName){
        List<Family> families = familyDao.findAll();
        int index = families.indexOf(family);
        Random random =new Random();
        boolean isBoy = random.nextBoolean();
        String  name = (isBoy? boyName : girlName);
        Family currentFamily = families.get(index);
        Calendar calendar = new GregorianCalendar();
        long birthDay = calendar.getTimeInMillis();

        currentFamily.addChild(new Human(name,currentFamily.getFather().getSurname(),birthDay ));
        return currentFamily;
    }
    // 14
    public void deleteAllChildrenOlderThen(int age){
        List<Family> families = familyDao.findAll();
        for(int i = 0;i < families.size();i++){
            List <Human> familyChildren = families.get(i).getChildren();
            Iterator<Human> itr = familyChildren.iterator();
            for (; itr.hasNext(); ) {
                Human child = itr.next();
                LocalDate birthDay = Instant.ofEpochMilli(child.getBirthdate())
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate();


                LocalDate now = LocalDate.now();
                Period period = Period.between(birthDay,now);

                if(period.getYears()  > age){
                itr.remove();}

            }
            }
        }
    }


