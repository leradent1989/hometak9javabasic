package hometask9;

import java.util.List;

 interface FamilyDao{

    void save(Family family);
    boolean deleteFamily(int index);
    List <Family> findAll();
    Family getFamilyByIndex(int index);
    boolean deleteFamily(Family family);

}

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> familyList;

    public CollectionFamilyDao(List<Family> familyList) {
        this.familyList = familyList;
    }

    public void save(Family family) {
        familyList.add(family);
    }
   public  boolean deleteFamily(int index){
        int length = familyList.size();
        familyList.remove(index);
        if(familyList.size() < length){
            return true;
        }else return false;
   }
    public  boolean deleteFamily(Family family){
        int length = familyList.size();
        familyList.remove(family);
        if(familyList.size() < length){
            return true;
        }else return false;
    }
    public List<Family> findAll() {
        return familyList;
    }

    public Family getFamilyByIndex(int index){
        if(index == -1 || index > familyList.size()){
            return  null;
        } else return familyList.get(index);
    }

}
